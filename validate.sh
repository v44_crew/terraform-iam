#!/bin/sh

dir_list=`ls ./modules/`

for i in ${dir_list}:
do
    cd /opt/atlassian/pipelines/agent/build/modules/${i}
    echo "--------------------------------------"
    echo "Changinging directories to ${i}"
    echo "--------------------------------------"
    terraform init 

    echo "--------------------------------------"
    echo "Running terraform validate"
    echo "--------------------------------------"
    terraform validate -check-variables=false
done