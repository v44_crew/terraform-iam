# AWS Identity and Access Management (IAM) Terraform module

These types of resources are supported:

* IAM account alias
* IAM password policy
* IAM user
* IAM user login profile
* IAM group
* IAM role
* IAM policy
* IAM access key
* IAM SSH public key

## Features

1. **Cross-account access.** Define IAM roles using `iam_assumable_role` or `iam_assumable_roles` submodules in "resource AWS accounts (prod, staging, dev)" and IAM groups and users using `iam-group-with-assumable-roles-policy` submodule in "IAM AWS Account" to setup access controls between accounts. 
1. **Individual IAM resources (users, roles, policies).** 

## Usage

`iam-account`:
```hcl
module "iam_account" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-account.git?ref=tags/v1.0.0"

  account_alias = "awesome-company"

  minimum_password_length = 37
  require_numbers         = false
}
```

`iam-assumable-role`:
```hcl
module "iam_assumable_role" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-assumable-role.git?ref=tags/v1.0.0"

  trusted_role_arns = [
    "arn:aws:iam::307990089504:root",
    "arn:aws:iam::835367859851:user/anton",
  ]

  create_role = true

  role_name         = "custom"
  role_requires_mfa = true

  custom_role_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonCognitoReadOnly",
    "arn:aws:iam::aws:policy/AlexaForBusinessFullAccess",
  ]
}
```

`iam-assumable-roles`:
```hcl
module "iam_assumable_roles" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-assumable-roles.git?ref=tags/v1.0.0"

  trusted_role_arns = [
    "arn:aws:iam::307990089504:root",
    "arn:aws:iam::835367859851:user/anton",
  ]

  create_admin_role = true

  create_poweruser_role = true
  poweruser_role_name   = "developer"

  create_readonly_role       = true
  readonly_role_requires_mfa = false
}
```

`iam-assumable-roles-with-saml`:
```hcl
module "iam_assumable_roles_with_saml" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-assumable-roles-with-saml.git?ref=tags/v1.0.0"

  create_admin_role = true

  create_poweruser_role = true
  poweruser_role_name   = "developer"

  create_readonly_role = true

  provider_name = "idp_saml"
  provider_id   = "arn:aws:iam::235367859851:saml-provider/idp_saml"
}
```

`iam-user`:
```hcl
module "iam_user" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-user.git?ref=tags/v1.0.0"

  name          = "vasya.pupkin"
  force_destroy = true

  pgp_key = "keybase:test"

  password_reset_required = false
}
```

`iam-policy`:
```hcl
module "iam_policy" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-policy.git?ref=tags/v1.0.0"

  name        = "example"
  path        = "/"
  description = "My example policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
```

`iam-group-with-assumable-roles-policy`:
```hcl
module "iam_group_with_assumable_roles_policy" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-group-with-assumable-roles-policy.git?ref=tags/v1.0.0"

  name = "production-readonly"

  assumable_roles = [
    "arn:aws:iam::835367859855:role/readonly"  # these roles can be created using `iam_assumable_roles` submodule
  ]
  
  group_users = [
    "user1",
    "user2"
  ]
}
```

`iam-group-with-policies`:
```hcl
module "iam_group_with_policies" {
  source = "git::https://bitbucket.org/v44_crew/terraform-iam/src/master/modules/iam-group-with-policies.git?ref=tags/v1.0.0"

  name = "superadmins"

  group_users = [
    "user1",
    "user2"
  ]

  attach_iam_self_management_policy = true

  custom_group_policy_arns = [
    "arn:aws:iam::aws:policy/AdministratorAccess",
  ]

  custom_group_policies = [
    {
      name   = "AllowS3Listing"
      policy = "${data.aws_iam_policy_document.sample.json}"
    }
  ]
}
```

Please note that the `ref` argument in the `source` URL should always be set to the latest version value. You can find the latest version by checking for the most recent tag in the repository.

## IAM Best Practices

AWS published [IAM Best Practices](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html) and this Terraform module was created to help with some of points listed there.