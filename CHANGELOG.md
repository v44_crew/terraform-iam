## v1.0.3 - 2019-10-23

- Add trusted service assume role feature

## v1.0.2 - 2019-09-12

- Update README.md

## v1.0.1 - 2018-08-28

- Updated pipeline to use validate.sh script

## v1.0.0 - 2018-08-27

- Pipeline initial
- Initial commit